const btnEl = document.getElementById("btn");
const inputEl = document.getElementById("input")

btnEl.addEventListener("click", calculateAge);
  
  function calculateAge() {
    const inputValue = inputEl.value;
    if (inputValue === "") {
      alert("Please enter your birthday");
    } else {
      const age = getAge(inputValue);
      const output = document.getElementById("result");
      output.innerText = `Your age is ${age} ${age > 1 ? "years": "year"} old`;
  }
}

function getAge(strDate) {
    const today = new Date();
    const birthday = new Date(strDate);
  let years = today.getFullYear() - birthday.getFullYear();
  if (today.getMonth() < birthday.getMonth() 
    || (today.getMonth() === birthday.getMonth() 
    && today.getDate() < birthday.getDate())) {
        years--;
    }
    return years
}

